package lv.zoover.homework

import java.io._
import java.nio.file.{Paths, Files}

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import org.apache.commons.net.ftp.{FTP, FTPClient}


object DataFileAdapter {
  def main(args: Array[String]): Unit = {

    val dataFileAdapter = new DataFileAdapter
    val ftpConnection = dataFileAdapter.createConnection()
    dataFileAdapter.download(ftpConnection)
    dataFileAdapter.unzipFiles()
    ftpConnection.disconnect()

  }
}

class DataFileAdapter {

  val config: Config = ConfigFactory.load()

  def createConnection(defaultRetryAmount: Int = 3): FTPClient = {
    val ftpClient = new FTPClient()
    ftpClient.connect(config.getString("server"))
    ftpClient.login(config.getString("username"), config.getString("password"))
    ftpClient.enterLocalPassiveMode()
    ftpClient.changeWorkingDirectory("incoming")
    ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
    ftpClient
  }

  def download(ftpClient: FTPClient): Option[Boolean] = {
    ftpClient.listFiles().filter(_.isFile).foreach { remoteDataFile =>
      println(s"Downloading ${remoteDataFile.getName}")
      var outputStream: OutputStream = null
      try {
        outputStream = new BufferedOutputStream (new FileOutputStream(config.getString("outputFolder") + remoteDataFile.getName))
        ftpClient.retrieveFile(remoteDataFile.getName, outputStream)
      } catch {
        case e: IOException => {
          println(s"Input output error while downloading ${remoteDataFile.getName}" + e.getMessage)
        }
        case e: Exception => {
          println(e.getMessage)
          None
        }

      } finally {
        outputStream.close()
      }
    }
    Some(true)
  }


  def unzipFiles(path: String = config.getString("outputFolder"), extractPath: String = config.getString("extractFolder")): Option[Boolean] = {
    val extractFolder = new File(extractPath)
    if (!extractFolder.exists())
      extractFolder.mkdir()

    new java.io.File(path).listFiles.filter(f =>f.isFile && f.getName.endsWith("bz2"))
      .foreach { file =>
        println(s"Unzipping ${file.getName}")

        val bzip2Is = new BZip2CompressorInputStream(new FileInputStream(file))

        val extractFile = extractPath + file.getName.substring(0, file.getName.length - 4)
        try {
          Files.copy(bzip2Is,Paths.get(extractFile))
        } catch {
          case e: IOException => println(e.getMessage)
        } finally {
          bzip2Is.close()
        }
      }

    Some(true)
  }

  @annotation.tailrec
  final def retry[T](n: Int)(fn: => T): T = {
    util.Try(fn) match {
      case util.Success(x) => x
      case _ if n > 1 => retry(n - 1)(fn)
      case util.Failure(e) => throw e
    }
  }
}
