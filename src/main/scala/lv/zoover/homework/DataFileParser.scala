package lv.zoover.homework


import com.github.tototoshi.csv.{CSVReader, DefaultCSVFormat}
import lv.zoover.homework.DataFileParser._
import org.joda.time.{LocalTime, DateTime}
import org.joda.time.format.{DateTimeFormatter, DateTimeFormat}

object DataFileParser {
  val Location = "LOCATION"
  val Date = "DATE"
  val Time = "TIME"
  val LeadTime = "LEADTIME"
  val ObservationLeadTime = "OBS"

}

class DataFileParser {

  implicit object SemiColonFormat extends DefaultCSVFormat {
    override val delimiter = ';'
  }

  def csvDataFromFile(filename: String): List[Map[String, String]] = {
    val reader = CSVReader.open(filename)
    val allWithHeaders: List[Map[String, String]] = reader.allWithHeaders()
    allWithHeaders
  }

  def trimMap(mapToTrim: Map[String, String]): Map[String, String] = {
    mapToTrim map {
      case (key, value) => (key.trim, value.trim)
    }
  }

  def resultData(cleanedUpData: List[Map[String, String]]) = {
    cleanedUpData.filter(_.get(LeadTime).get != ObservationLeadTime)
      .map { csvrow =>
        val uniqueId = calculateUniqueId(csvrow)
        val forecastTime = calculateForecastTime(csvrow)
        val resultTuple = (uniqueId, forecastTime, csvrow)
        resultTuple
      }
  }

  def calculateForecastTime(csvRow: Map[String, String]): String = {
    val date = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(csvRow.get(Date).get)
    val leadTime = csvRow.get(LeadTime).get
    val time = LocalTime.parse(csvRow.get(Time).get).plusHours(leadTime.substring(0, 3).toInt)
    date.withTime(time).toString("yyyy-MM-dd HH:mm")
  }

  def calculateUniqueId(csvRow: Map[String, String]): String = {
    csvRow.get(Location).get + csvRow.get(Date).get + csvRow.get(Time).get + csvRow.get(LeadTime).get
  }
}
