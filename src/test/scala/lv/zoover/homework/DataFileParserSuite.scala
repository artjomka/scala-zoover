package lv.zoover.homework

import lv.zoover.homework.DataFileParser._
import org.scalatest.FunSuite

import scala.collection.immutable.HashMap

class DataFileParserSuite extends FunSuite {
  val dataFileParser = new DataFileParser

  test("Trim csv row map key and values  ") {

    val result: Map[String, String] = dataFileParser.trimMap(HashMap(" 1 " -> " hello", "2 " -> "world", "3" -> " kitty"))
    assert(result.get("1").get == "hello")
    assert(result.get("2").get == "world")
    assert(result.get("3").get == "kitty")
  }

  test("Process lists of csv row maps to return be resultData") {
    val resultData: List[(String, String, Map[String, String])] = dataFileParser.resultData(List(
      HashMap(Location -> "062100", Date -> "2015-09-03", Time -> "16:00", LeadTime -> "OBS"),
      HashMap(Location -> "062250", Date -> "2015-09-04", Time -> "16:30", LeadTime -> "00100")))
    assert(resultData.size == 1)
    assert(resultData(0)._1 == "0622502015-09-0416:3000100")
    assert(resultData(0)._2 == "2015-09-04 17:30")
    assert(resultData(0)._3 == HashMap(Location -> "062250", Date -> "2015-09-04", Time -> "16:30", LeadTime -> "00100"))
  }

  test("Calculate unique id ") {
    val uniqueId = dataFileParser.calculateUniqueId(HashMap(Location -> "062100", Date -> "2015-09-03", Time -> "16:00", LeadTime -> "OBS"))
    assert(uniqueId == "0621002015-09-0316:00OBS")
  }

  test("Calculate forecast time") {
    val forecastTime = dataFileParser.calculateForecastTime(HashMap(Date -> "2015-09-04", Time -> "16:30", LeadTime -> "00200"))
    assert(forecastTime == "2015-09-04 18:30")
  }

}
